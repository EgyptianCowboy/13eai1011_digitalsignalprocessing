from __future__ import print_function, division

import thinkdsp
import thinkplot
import thinkstats2

import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings('ignore')

from ipywidgets import interact, interactive, fixed
import ipywidgets as widgets

df = pd.read_csv('thingsboard_data.csv')
df.set_index('created_at', inplace=True)
print(df)
df.plot()
df.show()
